## Group name

PA industry

## Group Members

- Pathomsakul Supamanee 612115002
- Pakin Kampeera 612115005

## Email addresses

- pathomsakul_s@elearning.cmu.ac.th
- armpakin@hotmail.com

## URL to webpage

http://3.88.252.221:8081/

## ip:port for backend

3.88.252.221:8082

## Katalon test repository link

https://gitlab.com/Pakin-Kampeera/se234-katalon-test
